package com.example

import com.typesafe.config.Config

class TransactionManager(
  config: Config
) {
  val MinTxnCount = config.getInt("min-transaction-count")
  val NumMerchantsReturned = config.getInt("num-merchants-returned")

  // map of (userId -> map(merchantId -> map of transactions))
  var transactions = Map.empty[Int, Map[String, Map[Int, Transaction]]]

  def getTopMerchantsForUser(userId: Int): Option[List[String]] = {
    transactions.get(userId) match {
      case None =>
        // given user does not exist
        None
      case Some(txns) =>
        val txCount = txns.foldLeft(0)((count, kv) => count + kv._2.values.size)
        if (txCount < MinTxnCount) {
          // not enough txns for this user
          Some(List.empty)
        }
        else {
          // sort merchants by txn count and return the top 3 merchant names w/ the most txns
          val topMerchants = txns.toList.sortBy(kv => kv._2.values.size).reverse.take(NumMerchantsReturned).map(_._1)
          Some(topMerchants)
        }
    }
  }

  def updateUserTransactions(tx: Transaction) = {
    // find the user
    val userTxns = transactions.get(tx.userId) match {
      case None =>
        // create a new bucket for this user
        Map(tx.merchant -> Map.empty[Int, Transaction])
      case Some(userTransactionsByMerchant) => userTransactionsByMerchant
    }

    // find the txns for the merchant on this txn
    val txnsByTxnId: Map[Int, Transaction] = userTxns.get(tx.merchant) match {
      case None =>
        // create a new bucket for this merchant
        Map(tx.txId -> tx)
      case Some(merchantTransactions) =>
        merchantTransactions + (tx.txId -> tx)
    }

    val updatedTxns = (tx.userId -> (userTxns + (tx.merchant -> txnsByTxnId)))
    //println(s"updated user transactions: updatedTxns")
    transactions = transactions + updatedTxns
  }
}
