package com.example

import java.net.InetAddress
import java.time.ZonedDateTime

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server._
import akka.stream.{ActorMaterializer, Materializer}
import com.typesafe.config.Config
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import net.ceedubs.ficus.Ficus._

import scala.concurrent.ExecutionContextExecutor
import scala.util.{Failure, Success, Try}

case class Transaction(
  userId: Int,
  merchant: String,
  price: Double,
  purchaseDate: ZonedDateTime,
  txId: Int
)

class Main(
  config: Config,
  transactionManager: TransactionManager)
(implicit val system: ActorSystem, val materializer: Materializer) {
  implicit val ec: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global
  import Directives._
  import io.circe.generic.auto._

  val postUserTransaction: Route = (decodeRequest & encodeResponse) {
    pathPrefix("transaction") {
      post {
        entity(as[Transaction]) { tx =>
          transactionManager.updateUserTransactions(tx)
          complete { OK }
        }
      }
    }
  }

  val getUserTransactionsRoute: Route = (decodeRequest & encodeResponse) {
    pathPrefix("transactions" / IntNumber) { userId =>
      get {
        transactionManager.getTopMerchantsForUser(userId) match {
          case None => complete(NotFound -> "no user found for given id")
          case Some(topMerchants) =>
            if (topMerchants.isEmpty) {
              complete(NoContent -> "not enough transactions for user")
            }
            else {
              complete(OK -> topMerchants)
            }
        }
      }
    }
  }

  val routes = postUserTransaction ~ getUserTransactionsRoute
}

object Main {
  def main(args: Array[String]): Unit = {
    System.setProperty("HOSTNAME", InetAddress.getLocalHost.getHostName)

    implicit val system       = ActorSystem()
    implicit val materializer = ActorMaterializer()
    implicit val ec: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global
    Try({
      // TODO pureconfig would be nicer for this
      val config = system.settings.config.as[Config]("services")
      val host   = config.getString("host")
      val port   = config.getInt("port")
      val txMgr  = new TransactionManager(config)
      val svc    = new Main(config, txMgr)
      val route  = svc.routes

      Http().bindAndHandle(route, host, port).onComplete {
        case Success(_) =>
          system.log.info(s"started transaction tracking service on $host:$port")
        case Failure(e) =>
          terminate(s"failed to bind to $host:$port b/c: $e")
      }
    }).failed.foreach(e => terminate(e.getMessage))
  }

  private def terminate(message: String)(implicit system: ActorSystem) = {
    system.log.error(message)
    system.terminate()
  }
}
