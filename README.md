# SoFi Exercise

a set of REST endpoints for storing and retrieving user transactions. two endpoints are defined:
 
- a POST endpoint, `/transaction`, for storing a user transaction
- a GET endpoint, `/transactions/<userId>` for retrieving a user's top most-frequently visited merchants, where `num-merchants-returned` is the number of merchants returned

NOTE:

- the GET endpoint will only return results when there are at least `min-transaction-count` transactions for the given user.
- if the above condition is met and there are fewer than `num-merchants-returned` merchants associated with a given user, then only the existing merchant names are returned. 

## Running / Interacting

to start the services:

`sbt run`

once running, load the sample data:

`./loadData.sh`

the following script will invoke the GET endpoint against a few known userId's:

`./verify.sh`

the endpoints can be invoked like so:

- `http GET http://localhost:9091/transactions/123`
- `http POST http://localhost:9091/transaction userId=123 merchant=foo price=1.45 purchaseDate=2019-03-01T07:34:56Z txId=999`

