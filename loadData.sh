#!/bin/bash
#http POST localhost:9091/transaction userId=123 merchant=foo price=2.34 purchaseDate=2019-04-01T12:34:56Z txId=444
#http POST localhost:9091/transaction userId=123 merchant=foo price=32.1 purchaseDate=2019-04-01T12:34:56Z txId=555
#http POST localhost:9091/transaction userId=123 merchant=bar price=1.23 purchaseDate=2019-03-01T12:34:56Z txId=1
#http POST localhost:9091/transaction userId=123 merchant=bar price=2.34 purchaseDate=2019-03-01T00:34:56Z txId=2
#http POST localhost:9091/transaction userId=123 merchant=bar price=3.45 purchaseDate=2019-03-01T07:34:56Z txId=3
#http POST localhost:9091/transaction userId=123 merchant=bar price=4.56 purchaseDate=2019-03-01T07:34:56Z txId=4
#http POST localhost:9091/transaction userId=123 merchant=baz price=1.23 purchaseDate=2019-03-01T07:34:56Z txId=888
#http POST localhost:9091/transaction userId=123 merchant=baz price=12.34 purchaseDate=2019-03-01T07:34:56Z txId=999
#http POST localhost:9091/transaction userId=123 merchant=qux price=1.23 purchaseDate=2019-03-01T07:34:56Z txId=999

#http POST localhost:9091/transaction userId=321 merchant=foo price=1.23 purchaseDate=2019-03-01T07:34:56Z txId=1
#http POST localhost:9091/transaction userId=321 merchant=baz price=12.34 purchaseDate=2019-03-01T07:34:56Z txId=2

awk -F "," '{cmd="userId="$1 " merchant=\""$2"\"" " price="$4 " purchaseDate=\""$5"Z\" txId="$6  ; print("http --ignore-stdin POST localhost:9091/transaction " cmd)}' src/main/resources/coding_challenge_data.csv | sh
