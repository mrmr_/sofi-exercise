name := "sofi-exercise"
organization := "foo"
version := "0.1"
scalaVersion := "2.12.16"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")
scalacOptions ++= Seq("-Xmax-classfile-name","78")

libraryDependencies ++= {
  val akka        = "2.5.22"
  val akkaHttp    = "10.1.8"
  val log4j       = "2.8.2"
  val scalaTestV  = "3.0.1"
  val circeVersion = "0.11.1"

  Seq(
    "com.typesafe.akka" %% "akka-actor" % akka,
    "com.typesafe.akka" %% "akka-stream" % akka,
    "com.typesafe.akka" %% "akka-testkit" % akka,
    "com.typesafe.akka" %% "akka-http" % akkaHttp,
    "de.heikoseeberger" %% "akka-http-circe" % "1.25.2",
    "com.typesafe.akka" %% "akka-http-testkit" % akkaHttp,
    "org.scalatest"     %% "scalatest" % scalaTestV % "test",
    "com.outworkers"    %%  "phantom-dsl" % "2.7.5",
    "com.outworkers"    %%  "util-testing" % "0.30.1",

    "org.apache.logging.log4j"   % "log4j-core" % log4j,
    "org.apache.logging.log4j"   % "log4j-slf4j-impl" % log4j,
    "com.fasterxml.jackson.core" % "jackson-databind" % "2.8.7", // for using Log4j2's JsonLayout
    "de.heikoseeberger"          %% "akka-log4j" % "1.4.0",
    "org.apache.logging.log4j"   % "log4j-1.2-api" % "2.8.1",
    "com.iheart"                 %% "ficus" % "1.4.5",

    "io.circe"          %% "circe-core"                   % circeVersion,
    "io.circe"          %% "circe-generic"                % circeVersion,
    "io.circe"          %% "circe-parser"                 % circeVersion
  )
}

